# hsfilewatcher

A personal haskell practice to monitor files/directories and run commands when
they are changed

## Install

```bash
stack install
```

## Execute

```bash
# hsfilewatcher <path> -- <cmd>
hsfilewatcher ./ -- ls -alh
```
