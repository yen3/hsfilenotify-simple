{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}

module Main where

import           Control.Concurrent
import           Control.Monad.Reader
import qualified Data.Text              as T
import           System.Environment     (getArgs)
import           System.Exit
import           System.Process         (callCommand)

import           Control.Monad.Logger
import           System.FSNotify

import qualified FileWatcher.Cmd        as C
import           FileWatcher.Concurrent
import           FileWatcher.Logging    (runTimeStderrLoggingT)
import           FileWatcher.TopLevel   (toplevelExceptionHandler)

data AppEnvironment = AppEnvironment
  { monitorPath :: String
  , monitorCmd  :: [String]
  }
  deriving (Show, Eq)

newtype App a = App { unApp :: ReaderT AppEnvironment (LoggingT IO) a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadReader AppEnvironment, MonadLogger)

runApp :: AppEnvironment -> App a -> IO a
runApp env appAction = runTimeStderrLoggingT $
  runReaderT (unApp appAction) env

callCommandOnChange :: String -> MVar () -> IO ()
callCommandOnChange rs m =
  runTimeStderrLoggingT $ do
    liftIO $ debounceTakeMVar m 500000  -- 0.5 sec

    logInfoN $ "Start to run command" <> T.pack rs

    liftIO $ do
      callCommand rs
      callCommandOnChange rs m

showEnv :: App ()
showEnv =
  do
    env <- ask
    logInfoN $ "env: " <> T.pack (show env)

watchFiles :: App ()
watchFiles =
  do
    env <- ask
    let cs = T.unpack $ T.intercalate " " (fmap T.pack (monitorCmd env))
    m <- liftIO newEmptyMVar
    _ <- liftIO $ forkTask (callCommandOnChange cs m)
    liftIO $ withManager $ \mgr ->
      do
        _ <- watchDir mgr (monitorPath env) (const True) (\_ -> putMVar m ())
        forever $ threadDelay 1000000

app :: App ()
app = do
  showEnv
  watchFiles

main :: IO ()
main = toplevelExceptionHandler $
  do
    parseResult <- C.parseCmdArgs <$> getArgs

    case parseResult of
      Just (cmdArgs, runCmd) -> runApp (AppEnvironment (C.path cmdArgs) runCmd) app
      Nothing -> putStrLn "Wrong args" >> exitWith (ExitFailure 2)
