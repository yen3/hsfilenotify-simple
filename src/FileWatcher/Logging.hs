module FileWatcher.Logging (
    runTimeStderrLoggingT
) where

import           Control.Monad.IO.Class (MonadIO)
import qualified Data.ByteString.Char8  as S8
import           Data.Time              (getZonedTime)
import           System.IO              (Handle, stderr)

import           Control.Monad.Logger

timestampDefaultOutput :: Handle
                       -> Loc
                       -> LogSource
                       -> LogLevel
                       -> LogStr
                       -> IO ()
timestampDefaultOutput h loc src level msg =
  do
    ts <- show <$> getZonedTime
    S8.hPutStr h (S8.concat $ S8.pack <$> ["[", ts, "]"])
    S8.hPutStr h ls
  where
    ls = defaultLogStrBS loc src level msg

defaultLogStrBS :: Loc
                -> LogSource
                -> LogLevel
                -> LogStr
                -> S8.ByteString
defaultLogStrBS a b c d =
    fromLogStr $ defaultLogStr a b c d
  where
    toBS = fromLogStr

runTimeStderrLoggingT :: MonadIO m => LoggingT m a -> m a
runTimeStderrLoggingT = (`runLoggingT` timestampDefaultOutput stderr)
