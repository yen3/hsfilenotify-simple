{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module FileWatcher.Cmd where

import           Data.Maybe      (fromJust, isNothing)
import qualified Data.Text       as T

import           Options.Generic

newtype CmdArgs = CmdArgs
  { path :: String
  }
  deriving (Show, Eq, Generic)

instance ParseRecord CmdArgs

splitArgs :: [String] -> String -> ([String], [String])
splitArgs xs sp =
  let rs = dropWhile (/= sp) xs
      fs = takeWhile (/= sp) xs
  in
    if null rs then (fs, rs) else (fs, tail rs)

parseCmdArgs :: [String] -> Maybe (CmdArgs, [String])
parseCmdArgs rs =
  let (rawCmdArgs, runCmd) = splitArgs rs "--"
      cmdArgs = getRecordPure (fmap T.pack rawCmdArgs)
  in
    if isNothing cmdArgs || null runCmd
       then Nothing
       else Just (fromJust cmdArgs, runCmd)
