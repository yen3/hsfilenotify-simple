module FileWatcher.Concurrent where

import           Control.Concurrent
import           Control.Concurrent.Chan as Chan
import           Control.Exception       (SomeException, mask, try)
import           Control.Monad           (when)
import           Data.Maybe              (isNothing, fromJust)
import           Data.Time.Clock.POSIX   (getPOSIXTime)
import           System.Timeout          (timeout)

forkFinallyOS ::  IO a -> (Either SomeException a -> IO ()) -> IO ThreadId
forkFinallyOS action and_then =
  mask $ \restore ->
    forkOS $ try (restore action) >>= and_then

-- | Get the current time. unit: microsecond(s)
getTime :: IO Integer
getTime = round . (* 1000000) <$> getPOSIXTime

{- | fork a joinable thread

__Examples:__

@
import           Control.Concurrent

main = do
  jv1 <- forkTask $ do
    putStrLn "Hello, world1"
  takeMVar jv1
@
-}
forkTask :: IO () -> IO (MVar ())
forkTask f = do
  var <- newEmptyMVar
  forkFinally f (\_ -> putMVar var ())
  return var

-- | Take a value from MVar with non-blocking mode
-- If it returns Nothing, sleep for a while
tryTakeMVarSleep ::
    MVar a           -- The MVar
    -> Integer       -- Sleep time (unit: microseconds)
    -> IO (Maybe a)  -- The value from MVar
tryTakeMVarSleep m t =
  do
    r <- tryTakeMVar m
    when (isNothing r) $ threadDelay $ fromIntegral t
    return r

-- | Debounce the multiple mvar in a time duration
debounceTakeMVar' ::
    Maybe a         -- Initial status. Put Nothing by default
    -> MVar a       -- MVar
    -> Integer      -- The time duration (unit: microseconds)
    -> IO (Maybe a) -- Final status
debounceTakeMVar' s m t =
  do
    startTime <- getTime
    r <- tryTakeMVarSleep m 1000
    endTime <- getTime

    let n = merge s r
        rt = t - (endTime - startTime)

    if rt < 0
       then return n
       else debounceTakeMVar' n m rt

  where merge ma mb =
            case (ma, mb) of
              (Just _, Just b)  -> Just b
              (Nothing, Just b) -> Just b
              (Just a, Nothing) -> Just a
              _                 -> Nothing

-- | Debounce the multiple mvar in a time duration
debounceTakeMVar ::
    MVar a       -- MVar
    -> Integer   -- The time duration (unit: microseconds)
    -> IO a      -- Final status
debounceTakeMVar m t =
  do
    s <- takeMVar m
    r <- debounceTakeMVar' (Just s) m t
    return $ fromJust r
